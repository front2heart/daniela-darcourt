$(window).on("load", function() {
  $('.loading').fadeOut(500);
});
$("#start-step-02").on("click", function() {
  $("#step-02").show();
  $("#step-02").addClass("animated slideInDown");
  setTimeout(function() {
    $("#step-02 .heart").show();
    $("#step-02 .heart").addClass("animated zoomIn");
    $("#corazon").trigger("play");
  }, 1000);
});

$("#start-end").on("click", function() {
  $("#step-02 .cont-info").addClass("animated zoomOut");
  $("#step-02 .heart").addClass("heart-active");
  $("#step-02 .cont-result").css("display", "table");
  $("#corazon").trigger("pause");
  $("#corazon-rapido").trigger("play");
  setTimeout(function() {
    $(".load-result").addClass("load-result-ok");
    $(".number").show();
    calculo();
    $("#step-02 .heart img").addClass("heartBeat");
    setTimeout(function() {
      $("#step-02 .heart img").css("opacity", "0.3");
    }, 1000);
    setTimeout(function() {
      $(".result-all").show();
      $("#musica").trigger("play");
    }, 3000);
  }, 1000);
});

function calculo() {
  $(".number span").each(function() {
    $(this)
      .prop("Counter", 0)
      .animate(
        {
          Counter: $(this).text()
        },
        {
          duration: 3000,
          easing: "swing",
          step: function(now) {
            $(this).text(Math.ceil(now));
          }
        }
      );
  });
}
